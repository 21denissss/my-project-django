from django.contrib import admin

# Register your models here.
from .models import *
from user.models import User

admin.site.register(Question)
admin.site.register(Category)
admin.site.register(Answer)
admin.site.register(Comment)
admin.site.register(User)