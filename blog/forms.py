from django import forms
from .models import *

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('title', 'category',)

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('name',)

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('text',)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)