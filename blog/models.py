from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.urls import reverse,reverse_lazy

class Answer(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.text

class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Question(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    #comment = models.ForeignKey(Comment,on_delete=models.CASCADE)
    category = models.ManyToManyField(Category,blank=True,null=True,default=None)
    answer = models.OneToOneField(Answer,blank=True,null=True,on_delete=models.CASCADE)
    def get_absolute_url(self):
        return reverse('question_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    question = models.ForeignKey(Question,default=None,on_delete=models.SET_DEFAULT)

    def __str__(self):
        return self.text