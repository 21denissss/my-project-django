from django.urls import path
from .views import *

urlpatterns = [
    path('', QuestionsListView.as_view(), name='question_list'),
    #path('', views.BasedView.question_list, name='question_list'),
    path('question/<int:pk>/', QuestionDetailView.as_view(), name='question_detail'),
    path('question/new/', QuestionCreateView.as_view(), name='question_new'),
    path('question/<int:pk>/edit/', QuestionUpdateView.as_view(), name='question_edit'),
    path('question/answer/<int:pk>/edit/', AnswerCreateView.as_view(), name='answer_edit'),
    path('question/<int:pk>/delete/', QuestionDeleteView.as_view(), name='question_delete'),
    path('category/new/', CategoryCreateView.as_view(), name='category_new'),
    path('question/comment/<int:pk>/create/', CommentCreateView.as_view(), name='comment_create'),
]