from django.contrib.messages import success
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import *
from .forms import *
from django.urls import reverse,reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

class QuestionsListView(ListView):
    model = Question
    context_object_name = 'questions'
    template_name="blog/question_list.html"

    def get_queryset(self):
        # Получить запрос из формы
        query = self.request.GET.get('q')

        # Если запрос не пустой
        if query:
            # Вернуть отсортированные объекты
            return Question.objects.filter(category__name__icontains=query)

        # Вернуть все объекты
        return Question.objects.all()

class QuestionDetailView(DetailView):
    model = Question
    context_object_name = 'question'
    template_name="blog/question_detail.html"

    def get_context_data(self, **kwargs):
        context = super(QuestionDetailView, self).get_context_data(**kwargs)
        question = Question.objects.get(pk = self.kwargs['pk'])
        comments = Comment.objects.filter(question=question)
        context.update({'comments': comments})
        return context

class QuestionCreateView(CreateView):
    model = Question
    form_class = QuestionForm
    template_name = "blog/question_edit.html"
    success_url = '/'

    def post(self, request):
        # Получение текущей формы
        form = self.get_form()

        # Данные в форме действительные
        if form.is_valid():
            # Получение "вопроса" из формы
            question = form.save(commit=False)

            # Автор вопроса - пользователь, вошедший в систему
            question.author = request.user

            # Дата публикации - текущая дата
            question.published_date = timezone.now()

            # Сохранение полей "1:М" и "1:1"
            question.save()

            # Отдельное сохранение полей "М:М"
            form.save_m2m()

            # Перенаправление на подробную информацию о данном "вопросе"
            return redirect('question_detail', pk=question.pk)

        # Перейти к редактированию вопроса
        return render(request, 'blog/question_edit.html', {'form': form})

class QuestionUpdateView(UpdateView):
    model = Question
    form_class = QuestionForm
    template_name = "blog/question_edit.html"

class QuestionDeleteView(DeleteView):
    model = Question
    success_url = reverse_lazy("question_list")

    def post(self, request, *args, **kwargs):
        # Получить текущий "вопрос"
        question = Question.objects.get(pk=self.kwargs['pk'])

        # Получить "комментарии" у "вопроса"
        comments = Comment.objects.filter(question=question)

        # Удалить
        for comment in comments:
            comment.delete()
        question.delete()

        # Перейти к списку "вопросов"
        return redirect('question_list')

class CategoryCreateView(CreateView):
    model = Category
    form_class = CategoryForm
    template_name = 'blog/category_new.html'
    success_url = '/'

class AnswerCreateView(CreateView):
    model = Answer
    form_class = AnswerForm
    template_name = 'blog/answer_edit.html'
    success_url = '/'

    def post(self, request, pk):
        # Получение текущей формы
        form = self.get_form()

        # Данные в форме действительные
        if form.is_valid():
            # Получение "ответа" из формы
            answer = form.save(commit=False)

            # Автор вопроса - пользователь, вошедший в систему
            answer.author = request.user

            # Дата публикации - текущая дата
            answer.published_date = timezone.now()

            # Сохранение полей "1:М" и "1:1"
            answer.save()

            # Получение "вопроса" для задания ему "ответа"
            question = Question.objects.get(pk=pk)

            # "Ответ" на "вопрос" - текущий созданный "ответ"
            question.answer = answer

            # Сохранение полей "1:М" и "1:1"
            question.save()

            # Перенаправление на подробную информацию о данном "вопросе"
            return redirect('question_detail', pk=pk)

        # Перейти к редактированию ответа
        return render(request, 'blog/answer_edit.html', {'form': form})

class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'blog/comment_create.html'
    success_url = '/'

    def post(self, request, pk):
        # Получение текущей формы
        form = self.get_form()

        # Данные в форме действительные
        if form.is_valid():
            # Получение "комментария" из формы
            comment = form.save(commit = False)

            # Автор комментария - пользователь, вошедший в систему
            comment.author = request.user

            # Дата публикации - текущая дата
            comment.created_date = timezone.now()

            # Вопрос, к которому принадлежит комментарий
            comment.question = Question.objects.get(pk=pk)

            # Сохранение полей "1:М" и "1:1"
            comment.save()

            # Перенаправление на подробную информацию о данном "вопросе"
            return redirect('question_detail', pk=pk)

        # Перейти к редактированию комментария
        return render(request, 'blog/comment_create.html', {'form': form})