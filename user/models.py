from django.conf import settings
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from blog.models import Question
from django.urls import reverse,reverse_lazy

# Create your models here.

class User(AbstractUser):
    #userQuestion = models.ForeignKey(Question,on_delete=models.CASCADE,default=None)
    isManager = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('question_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.username