from django.contrib.messages import success
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import *
from django.urls import reverse,reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

class UserUpdateView(UpdateView):
    model = User
    fields = ('isManager',)
    template_name = "user_update.html"
    success_url = '/'